# Creating 3D models

We are using Blender for 3D model creating, and only Blender. Using only one software helps keeping track of everything and makes the development simpler. To prevent issues, please try to use a version as close as possible as the one mentioned in the main [README](../README.md).

## Exporting models

No export is needed!

All created 3D models should be in the `.blend` format (default Blender file) and saved in the `/models` folder (or in a sub-folder). Godot will [automatically import `.blend` files](https://docs.godotengine.org/en/stable/tutorials/assets_pipeline/importing_3d_scenes/available_formats.html#importing-blend-files-directly-within-godot) if you followed the [installation guide](../README.md).

## Scale

1 Godot unit is 1 blender unit.
