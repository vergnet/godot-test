# Documentation

Welcome to the developers documentation. Here you will find some information to help you contribute to the project.

- [How to use Git](VCS.md)
- [Creating 3D models](BLENDER.md)
- [Game rules](GAME_RULES.md)
