# Game rules

This document is a work in progress. The rules are not fully worked out yet.

The following explains in as much details as possible the game rules, from the game setup to the win conditions.

## Setup

### Players

The game is multiplayer and does not have any AI controlled characters. The exception is some devices placed by the player which can target other players automatically when entering their range.

### Teams

There are 2 teams: the **Infiltrators** and the **Security**. The infiltrators are attacking a complex, the security is defending it. The security team consists of only one player.

### Map

The map is a complex consisting only of interior rooms, there is no outside. The map is nearly fully black and users need to light their way through.

The complex is procedurally generated from pre-made rooms. A basic room block is 10 units by 10 units. Bigger rooms may exist and will be fitted inside several blocks of 10 units.

### Lobby

Upon joining a lobby, players are represented as SD cards plugging in to a port. Once all players are ready, one is chosen as the security player and will turn red.

### End

When the game ends, players go back to the lobby and the losing players SD cards get destroyed and replaced by new ones. The game can restart from there.

## Infiltrators

### Goal

Infiltrators must steal data from terminals in the given time. Terminals are scattered randomly across the map (in any map block) and may contain various amounts of data.

Once the quota is reached, infiltrators win.

### Start

All infiltrators start in their safe zone. This zone is separate from the complex and the security cannot reach it. This zone can be used to safely discuss strategies.
In the spawn, infiltrators can deposit stolen data, buy upgrades/items, heal and revive dead infiltrators (costly).

### Uplink zones

Uplink zones appear and change randomly (in any map block). Infiltrators can use those zones to leave the complex and go back to their safe zone.
Once in the safe zone, infiltrators can transfer their data to be added to the shared goal.

The spawn contains a single teleporter which will randomly teleport infiltrators to an uplink zone in the complex.

The security cannot see those zones.

### Death

Infiltrators have a health meter. Once it reaches 0, the infiltrator dies.
If an Infiltrator dies, they will drop all their stolen data and their memory card. They will have to spectate the other infiltrators for the rest of the game or until revived.

To regenerate health, infiltrators will need to extract to their safe zone and use the healing station.

To be revived, other infiltrators need to pick up the dead's memory card, bring it back to spawn and pay.

### Movement

Infiltrators can move around in first person. They can crouch and sprint.

Sprinting consumes energy. Once energy is depleted, sprinting is impossible. After a short time without sprinting, energy recharges itself.

Crouching makes the Infiltrator's hitbox smaller to allow them to move through tight passageways. (To be confirmed)

Infiltrators make a very faint noise when moving which can only be heard if very close.

### Vision

Infiltrators can see other infiltrators, their goal, the uplink zones and security devices if close enough. Their vision is greatly limited by the lack of light and by fog.

### Tools

Infiltrators do not have guns.

Depositing stolen data gives them money.
They can use money in the trade terminal to buy tools, heal or revive other infiltrators.

Infiltrators can buy one time use EMP charges to disable/destroy a security device.

Infiltrators can use a flashlight as the complex does not have any significant lights.

While in the safe zone, infiltrators can monitor the status (health, data carrying) of all other infiltrators.

## Security

### Goal

The security must prevent the infiltrators from reaching the data stolen quota.
Security wins if the timer runs out or if all infiltrators are dead.

The timer runs faster the more intact data terminal are remaining.

### Start

Security starts in a map view of the complex. They have no physical representation.

### Death

The security player cannot die, but the devices they control can be deactivated/destroyed.

### Movement

While in map view, the player can move around the complex and select security devices.

They can take control of some security devices, in which case they will go into first person view of that security device.

### Vision

Security can see all the security devices clearly. The whole map does not suffer from lack of light, there is no fog and no roof.

Security cannot see infiltrators on the map.
Security can see when a security device detects an infiltrator.

While controlling a security device, security will see in first person and will not need lights, but a fog will greatly limit the view distance.
While controlling a device in first person, security will be able to see infiltrators if they enter its field of vision.

When a data breach starts, the security is warned.

### Tools

Security has a few devices they can use.

Security earn credits generated by intact data terminals. The more terminals, the more money they get per second.
This money can be used to buy new security devices, new data terminals or repair stolen ones.
Newly placed devices will take a few seconds before activating.

All the following devices are randomly placed in the map on start. Security can still buy more:

- Mines will explode do massive damage to infiltrators. These cannot be controlled.
- Laser detectors will send an alarm when an infiltrator passes through it. These cannot be controlled. Infiltrators cannot see the laser beams but will see a light blip on detection.
- Cameras will send an alarm when an infiltrator enters its field of vision. These can be controlled for a 360 view. Infiltrators cannot see the camera's field of view, but security can see it.
- Turrets will send an alarm when an infiltrator enters its field of vision and shoot. These can be controlled for a 360 view and precise aiming, even without target. Infiltrators cannot see the camera's field of view, but security can see it.
- Doors can be locked or unlocked manually for a few seconds using money, while controlling a dispatch robot or when in the map view.

The following devices must be bought and are not placed in the map at the start:

- (Cheap) Mini drones can be used to move swiftly around the map and find infiltrators. These drones are entirely silent.
- (moderately expensive) Small dispatch robots can be controlled at will. These can melee attack infiltrators but will do nothing if not controlled. Makes a very faint noise when moving around which can only be heard if very close.
- (expensive) Large dispatch robots can be controlled at will. These can act as mobile turrets but will do nothing if not controlled. Makes noise when moving around.
