# How to use Git

The project uses [Git](https://git-scm.com/) as a version control system. If the words commit, amend, branch, merge, rebase, pull and push are unfamiliar to you, please check out a few tutorials such as https://learngitbranching.js.org/.

## Commits

The project follows the [conventional commit](https://www.conventionalcommits.org/en/v1.0.0/) specification. This helps reading the git log and generating game versions. If you are unsure, just look at examples from the git log. In any case we'll remind you about that in any Merge Request you create.

## Merge requests

All features and bug fixes should be done through Merge Requests. You should create an issue before creating any merge request. This allows us to discuss first, then code. Once you create your Merge request, please link the issue.

This is a volunteer project, so your merge requests may stay quite some time open, or not be merged at all. Such is the live of a hobby project.
