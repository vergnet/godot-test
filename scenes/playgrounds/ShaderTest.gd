extends Main

@onready var security_devices = $SecurityDevices

func _ready():
	super._ready()
	for c in security_devices.get_children():
		if "set_outlined" in c:
			c.set_outlined(true)

