class_name DataTerminal

extends Node3D

signal steal_data(player: int, terminal: DataTerminal)

@export var available_data = 100:
	set(value):
		if interactable:
			interactable.can_interact = value > 0
		available_data = value

@onready var interactable: Interactable = $Interactable


func _ready() -> void:
	interactable.can_interact = available_data > 0


func _process(_delta: float) -> void:
	if available_data > 0:
		interactable.look_at_hint = "Steal data (%s) [E]" % str(available_data)
	else:
		interactable.look_at_hint = "Terminal empty"


@rpc("any_peer", "call_local", "reliable")
func try_steal_data():
	var player = multiplayer.get_remote_sender_id()
	steal_data.emit(player, self)


func _on_interactable_interacted() -> void:
	try_steal_data.rpc()
