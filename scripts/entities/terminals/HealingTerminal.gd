class_name HealingTerminal

extends Node3D

signal request_heal(player: int, station: HealingTerminal)

@export var infiltrators_container: Node3D

@export var heal_amount = 10
@export var price = 10

@rpc("any_peer", "call_local", "reliable")
func heal():
	if multiplayer.is_server():
		var player = multiplayer.get_remote_sender_id()
		request_heal.emit(player, self)


func _on_interactable_interacted() -> void:
	heal.rpc()
