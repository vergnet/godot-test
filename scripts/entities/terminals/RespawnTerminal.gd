class_name RespawnTerminal

extends Node3D

signal request_respawn(player: int, terminal: RespawnTerminal)

@export var price = 100

@rpc("any_peer", "call_local", "reliable")
func respawn():
	if multiplayer.is_server():
		var player = multiplayer.get_remote_sender_id()
		request_respawn.emit(player, self)


func _on_interactable_interacted() -> void:
	respawn.rpc()
