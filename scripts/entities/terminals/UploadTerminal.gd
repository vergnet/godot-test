class_name UploadTerminal

extends Node3D

signal upload_data(player: int)

@rpc("any_peer", "call_local", "reliable")
func try_upload_data():
	var player = multiplayer.get_remote_sender_id()
	upload_data.emit(player)


func _on_interactable_interacted() -> void:
	try_upload_data.rpc()
