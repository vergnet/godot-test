extends Node3D

signal player_detected

@onready var light1 = $Lights/BlinkSecurityLight
@onready var light2 = $Lights/BlinkSecurityLight2
@onready var mouse_hover = $MouseHoverArea3D
@onready var debug_trigger = $DebugTrigger


func _ready():
	debug_trigger.visible = false


@rpc("any_peer", "call_local", "reliable")
func notify_position(player_position: Vector3):
	SignalBus.infiltrator_detected.emit(player_position)


func _on_area_3d_body_entered(body: Node3D):
	if body.is_in_group("Players"):
		player_detected.emit()
		light1.blink()
		light2.blink()
		notify_position.rpc(global_position)


func set_outlined(value: bool):
	mouse_hover.set_outlined(value)
	debug_trigger.visible = value
