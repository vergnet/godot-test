extends Node3D

@export var spawn_uplink_point: UplinkPoint


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	spawn_uplink_point.connect("teleport", _on_spawn_uplink_point_teleport)
	for u in get_children():
		u.connect("teleport", _on_uplink_point_teleport)


func _on_uplink_point_teleport(player: Infiltrator) -> void:
	player.infiltrating = false
	player.target_position = spawn_uplink_point.global_position


func _on_spawn_uplink_point_teleport(player: Infiltrator) -> void:
	player.infiltrating = true
	var uplink_count = get_child_count()
	var selected_uplink = randi_range(0, uplink_count - 1)
	var selected_uplink_position = (get_child(selected_uplink) as Node3D).global_position
	player.target_position = selected_uplink_position
