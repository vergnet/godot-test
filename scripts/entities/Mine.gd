extends Node3D

# Player will die if standing less than 2 units from the mine
const KILL_RANGE = 2
# Player will take no damage if farther than the max range
# Resize the damage area shape to change this
var max_range: int

@onready var damage_area = $DamageArea
@onready var damage_area_shape = $DamageArea/CollisionShape3D
@onready var explosion_particles = $ExplosionParticles
@onready var explosion_light = $ExplosionLight
@onready var explosion_light_timer = $ExplosionLight/LightTimer
@onready var mine_model = $mine
@onready var trigger_area = $TriggerArea
@onready var mouse_hover = $MouseHoverArea3D


func _ready():
	max_range = damage_area_shape.shape.radius


func _damage_players():
	if not multiplayer.is_server():
		return
	var bodies = damage_area.get_overlapping_bodies()
	for b in bodies:
		if b.is_in_group("Players"):
			var infiltrator: Infiltrator = b
			var distance = infiltrator.global_position.distance_to(damage_area.global_position)
			var damage = 0
			if distance < KILL_RANGE:
				damage = 100
			elif distance < max_range:
				damage = 100 * (max_range - distance) / (max_range - KILL_RANGE)
			if damage > 0:
				infiltrator.health -= damage


func _set_enabled(value: bool):
	mine_model.visible = value
	# Need to wait after the signal finished processing
	trigger_area.set_deferred("monitoring", value)


func _play_animation():
	explosion_particles.emitting = true
	explosion_light.visible = true
	explosion_light_timer.start()


@rpc("any_peer", "call_local", "reliable")
func explode():
	_set_enabled(false)
	_play_animation()
	_damage_players()
	SignalBus.infiltrator_detected.emit(global_position)
	# TODO remove after debug
	await get_tree().create_timer(5).timeout
	_set_enabled(true)


func _on_trigger_area_body_entered(body: Node3D):
	if body.is_in_group("Players"):
		explode.rpc()


func _on_light_timer_timeout():
	explosion_light.visible = false


func _on_smoke_particles_finished():
	# Smoke particles are the longest
	queue_free()


func set_outlined(value: bool):
	mouse_hover.set_outlined(value)
