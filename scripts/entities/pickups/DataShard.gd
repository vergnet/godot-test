class_name DataShard

extends Node3D

signal pickup_data(player: int, data_shard: DataShard)

@export var available_data: int
@export var players_contained: Array[int]

@onready var pickupable: Pickupable = $Pickupable


func _process(_delta: float) -> void:
	if len(players_contained) == 0:
		pickupable.look_at_hint = "Retrieve data (%s) [E]" % available_data
	else:
		pickupable.look_at_hint = (
			"Retrieve data (%s) and %d infiltrator cores [E]"
			% [available_data, len(players_contained)]
		)


func _on_pickupable_picked_up(infiltrator: Infiltrator) -> void:
	pickup_data.emit(infiltrator.player, self)


@rpc("any_peer", "call_local", "reliable")
func try_pickup_data():
	var player = multiplayer.get_remote_sender_id()
	pickup_data.emit(player, self)


func _on_pickupable_interacted() -> void:
	try_pickup_data.rpc()
