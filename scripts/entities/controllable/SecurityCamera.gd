extends SecurityFpsCharacter

signal player_detected

const AI_CONTROL_SPEED = 5

@export_category("Debug")
@export var show_viewport = false

@export_category("AI Camera")
@export var sweep_angle := 90
@export var sweep_time := 4

@export var ai_rotation_target: Vector3

var was_player_controlled = false

var focused_player: FpsCharacter
var tween: Tween
var _initial_camera_rotation: Vector3

@onready var blink_light = $FpsCameraWithAi/BlinkSecurityLight


func _ready():
	super()
	fps_camera.show_viewport = show_viewport
	tween = create_tween()
	fps_camera.show_viewport = show_viewport
	_initial_camera_rotation = fps_camera.rotation
	ai_rotation_target = _initial_camera_rotation
	setup_camera_animation()


func _process(delta: float) -> void:
	super(delta)
	if focused_player and not player_controlled:
		tween.stop()
		animated_look_at(focused_player.global_position)

	if player_controlled and tween.is_running():
		tween.stop()

	if was_player_controlled != player_controlled:
		_restart_tween()

	was_player_controlled = player_controlled


func _restart_tween():
	_initial_camera_rotation.y = fps_camera.rotation.y
	ai_rotation_target = _initial_camera_rotation
	tween = create_tween()
	setup_camera_animation()
	tween.play()


func _on_fps_camera_with_ai_player_focused(player: FpsCharacter):
	focused_player = player
	blink_light.blink()


func _on_fps_camera_with_ai_player_lost_focus() -> void:
	focused_player = null
	_restart_tween()


func set_outlined(value: bool):
	super.set_outlined(value)
	fps_camera.show_viewport = value


func get_rotation_target() -> Vector3:
	return player_input.rotation_target if player_controlled else ai_rotation_target


func get_control_speed():
	return CONTROL_SPEED if player_controlled else AI_CONTROL_SPEED


func setup_camera_animation():
	tween.tween_property(
		self,
		"ai_rotation_target:y",
		_initial_camera_rotation.y + deg_to_rad(sweep_angle) / 2,
		sweep_time / 2
	)
	tween.parallel().tween_property(
		self, "ai_rotation_target:x", _initial_camera_rotation.x, sweep_time / 2
	)
	tween.tween_property(self, "ai_rotation_target:y", _initial_camera_rotation.y, sweep_time / 2)
	tween.tween_property(
		self,
		"ai_rotation_target:y",
		_initial_camera_rotation.y - deg_to_rad(sweep_angle) / 2,
		sweep_time / 2
	)
	tween.tween_property(self, "ai_rotation_target:y", _initial_camera_rotation.y, sweep_time / 2)
	tween.set_loops()


func animated_look_at(target: Vector3):
	var origin = fps_camera.global_position
	var forward = target - origin
	var new_global_basis = Basis.looking_at(forward, Vector3.UP, true)
	ai_rotation_target = new_global_basis.get_euler()
