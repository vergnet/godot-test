class_name PlayableModel

extends Node3D

@export var x_model: MeshInstance3D
@export var y_model: MeshInstance3D

@export var offset_x = 0


func set_model_rotation(new_rotation: Vector3):
	x_model.rotation.x = new_rotation.x + deg_to_rad(offset_x)
	y_model.rotation.y = new_rotation.y


func get_model_rotation():
	return Vector3(x_model.rotation.x, y_model.rotation.y, 0)


func set_controlled(controlled: bool):
	visible = !controlled
