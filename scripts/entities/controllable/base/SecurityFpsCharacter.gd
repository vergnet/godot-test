class_name SecurityFpsCharacter

extends FpsCharacter

var id: int

@onready var mouse_hover = $MouseHoverArea3D
@onready var light = $SecurityLight


func _ready() -> void:
	super()
	id = UniqueIds.get_security_device_id()


func set_outlined(value: bool):
	if mouse_hover:
		mouse_hover.set_outlined(value)


func _process(delta: float) -> void:
	super(delta)
	light.enabled = _is_local_player_controlling()
