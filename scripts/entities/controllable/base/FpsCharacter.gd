class_name FpsCharacter

extends CharacterBody3D

signal health_changed(player: FpsCharacter)
signal sprint_energy_changed(energy: int)
signal look_at_object(object: LookAtHint)
signal stop_look_at_object
signal player_changed(player: FpsCharacter)

const INTERACT_DISTANCE = 1.5
const CROUCH_OFFSET = 0.4
const CONTROL_SPEED = 25

@export_category("Multiplayer")
@export var player_input: PlayerInput
@export var rollback_synchronizer: RollbackSynchronizer

@export var player_controlled = false:
	set(value):
		player_controlled = value
		player_input.enabled = value

@export var player: int:
	set(id):
		if id == 0:
			# This gets called when syncing the node using MultiplayerSpawner
			# There is a small delay between when the node is spawned and
			# the child MultiplayerSynchronizer can sync the value
			print("Cannot set player id to 0")
			return
		player = id
		# Give authority over the player input to the appropriate peer.
		player_input.set_multiplayer_authority(id)
		fps_camera.set_multiplayer_authority(id)
		rollback_synchronizer.process_settings()
		player_changed.emit(self)

@export_category("Health")
@export var has_health = true
@export var max_health = 100

@export_category("Sprint")
@export var can_sprint = true
@export var sprint_speed = 8.0

@export_category("Crouch")
@export var can_crouch = true
@export var crouch_speed = 2.0
@export var collision_shape_standing: CollisionShape3D
@export var collision_shape_crouched: CollisionShape3D

@export_category("Camera")
@export var fps_camera: FpsCamera
@export var model: PlayableModel
@export var fps_only: Node3D

@export_category("Movement")
@export var can_move = true
@export var walk_speed = 4.0
@export var air_speed = 1.0

@export var model_offset_x: float = 0
@export var model_offset_y: float = 0

@export var health: int:
	set(value):
		if health != value:
			health = value
			health_changed.emit(self)

# Get the gravity from the project settings to be synced with RigidBody nodes.
# But make the player a bit heavier
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity") + 10

var initial_head_height: float
var is_crouched = false
var can_stand_up = true
var can_restore_sprint = false
var was_sprinting = false
var spectating: bool

var sprint_energy: int:
	set(value):
		sprint_energy = value
		sprint_energy_changed.emit(sprint_energy)

var restore_sprint_timer: Timer

# Used to teleport player while still using the rollback function
var target_position = Vector3()
var interact_ray: RayCast3D

var aimed_at_object: Node3D:
	set(value):
		aimed_at_object = value
		if is_instance_of(value, LookAtHint):
			look_at_object.emit(value)
		else:
			stop_look_at_object.emit()


func _ready():
	fps_camera.global_rotation = player_input.rotation_target
	if not multiplayer.is_server():
		print(player_input.rotation_target)
	initial_head_height = fps_camera.position.y
	health = max_health
	sprint_energy = 100
	_set_model_rotation()
	_create_interact_ray()


func _process(delta: float) -> void:
	if _is_local_player_controlling() or spectating:
		Input.mouse_mode = Input.MOUSE_MODE_CAPTURED

	if fps_only:
		fps_only.visible = _is_local_player_controlling() or spectating
	_set_rotation(delta)
	_set_model_rotation()
	_crouch_player(delta)
	fps_camera.camera.current = _is_local_player_controlling() or spectating
	model.set_controlled(_is_local_player_controlling() or spectating)

	if player_input.interacting and _is_local_player_controlling():
		interact()

	player_input.interacting = false


func _physics_process(_delta: float) -> void:
	var collider = interact_ray.get_collider()
	if collider != aimed_at_object:
		aimed_at_object = collider


func _is_local_player_controlling():
	return player_controlled and player == multiplayer.get_unique_id()


func _rollback_tick(delta, _tick, _is_fresh):
	_move_player(delta)

	if not target_position.is_zero_approx():
		global_position = target_position
		target_position = Vector3()

	if position.y < -20:
		health = 0


func get_rotation_target() -> Vector3:
	return player_input.rotation_target


func get_control_speed():
	return CONTROL_SPEED


func _set_rotation(delta):
	var speed = get_control_speed()
	var rotation_target = get_rotation_target()
	fps_camera.global_rotation.x = lerp_angle(
		fps_camera.global_rotation.x, rotation_target.x, delta * speed
	)
	fps_camera.global_rotation.y = lerp_angle(
		fps_camera.global_rotation.y, rotation_target.y, delta * speed
	)


func _set_model_rotation():
	var model_rotation = Vector3(
		fps_camera.rotation.x + deg_to_rad(model_offset_x),
		fps_camera.rotation.y + deg_to_rad(model_offset_y),
		fps_camera.rotation.z
	)
	model.set_model_rotation(model_rotation)


func _get_speed():
	var speed = walk_speed
	var sprinting = _can_sprint() and player_input.sprinting
	if not _multiplayer_is_on_floor():
		speed = air_speed
	elif is_crouched:
		speed = crouch_speed
	elif sprinting:
		speed = sprint_speed
	return speed


func _get_target_velocity(direction: Vector3, speed: float):
	var target_velocity = velocity
	if direction:
		target_velocity.x = direction.x * speed
		target_velocity.z = direction.z * speed
		if not _multiplayer_is_on_floor():
			if abs(target_velocity.x) < abs(velocity.x):
				target_velocity.x = velocity.x
			if abs(target_velocity.z) < abs(velocity.z):
				target_velocity.z = velocity.z
	else:
		target_velocity.x = move_toward(target_velocity.x, 0, speed)
		target_velocity.z = move_toward(target_velocity.z, 0, speed)
	return target_velocity


func _get_vertical_velocity(delta):
	var vertical_velocity = velocity.y
	if not _multiplayer_is_on_floor():
		vertical_velocity -= gravity * delta
	return vertical_velocity


func _move_player(delta):
	if not _can_move():
		return
	var direction = player_input.direction
	var speed = _get_speed()
	_handle_energy(speed == sprint_speed and direction, delta)

	velocity = direction * speed
	velocity.y = _get_vertical_velocity(delta)
	velocity *= NetworkTime.physics_factor

	move_and_slide()


# See https://foxssake.github.io/netfox/netfox/tutorials/rollback-caveats/#characterbody-on-floor
func _multiplayer_is_on_floor():
	var old_velocity = velocity
	velocity = Vector3.ZERO
	move_and_slide()
	velocity = old_velocity
	return is_on_floor()


func _crouch_player(delta):
	if not _can_crouch():
		return
	var new_head_height = fps_camera.position.y
	if player_input.crouching and _multiplayer_is_on_floor():
		is_crouched = true
		new_head_height = initial_head_height - CROUCH_OFFSET
		_set_collisions_crouched(true)
	elif can_stand_up:
		is_crouched = false
		new_head_height = initial_head_height
		_set_collisions_crouched(false)
	if fps_camera.position.y != new_head_height:
		var crouch_speed = delta * 10
		fps_camera.position.y = lerp(fps_camera.position.y, new_head_height, crouch_speed)
	model.crouched = is_crouched


func _handle_energy(is_spriting: bool, delta):
	if not _can_recover_sprint():
		return

	if is_spriting:
		was_sprinting = true
		sprint_energy = move_toward(sprint_energy, 0, delta * 40)
	elif was_sprinting:
		was_sprinting = false
		_start_restore_sprint_timer()
	elif restore_sprint_timer and restore_sprint_timer.is_stopped() and sprint_energy < 100:
		sprint_energy = move_toward(sprint_energy, 100, delta * 30)


func _start_restore_sprint_timer():
	if restore_sprint_timer:
		restore_sprint_timer.stop()
		restore_sprint_timer.queue_free()
	restore_sprint_timer = Timer.new()
	restore_sprint_timer.wait_time = 3
	restore_sprint_timer.one_shot = true
	add_child(restore_sprint_timer)
	restore_sprint_timer.start()


func _can_move():
	return player_controlled and can_move


func _can_crouch():
	return _can_move() and can_crouch and _multiplayer_is_on_floor()


func _can_recover_sprint():
	return _can_move() and can_sprint


func _can_sprint():
	return _can_recover_sprint() and sprint_energy > 0


func _set_collisions_crouched(crouched: bool):
	if collision_shape_standing.disabled != crouched:
		collision_shape_standing.disabled = crouched
	if collision_shape_crouched.disabled == crouched:
		collision_shape_crouched.disabled = not crouched


func _create_interact_ray():
	interact_ray = RayCast3D.new()
	interact_ray.target_position = Vector3(0, 0, INTERACT_DISTANCE)
	interact_ray.collision_mask = 6  # layers 2 and 3 for interactable and pickable
	interact_ray.collide_with_areas = true
	fps_camera.add_child(interact_ray, true)


func interact():
	var collider = interact_ray.get_collider()
	if collider and collider.is_in_group("Interactable"):
		var target: Interactable = collider
		target.interact()


func reset_health():
	health = 100


func _on_area_3d_standing_body_entered(body):
	if body != self:
		can_stand_up = false


func _on_area_3d_standing_body_exited(body):
	if body != self:
		can_stand_up = true
