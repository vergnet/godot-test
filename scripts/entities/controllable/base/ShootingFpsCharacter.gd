class_name ShootingFpsCharacter

extends SecurityFpsCharacter

const SPRAY := 4

@export_category("Damage")
@export var hit_damage = 10
@export var shoot_cooldown = 0.2

var shooting := false
var in_cooldown := false

@onready var debug_ray: RayDebug = $RayDebug


func _physics_process(_delta):
	if _is_local_player_controlling():
		shooting = Input.is_action_pressed("shoot")
	if shooting and not in_cooldown:
		in_cooldown = true
		shoot.rpc()
		await get_tree().create_timer(shoot_cooldown).timeout
		in_cooldown = false


@rpc("any_peer", "call_local", "reliable")
func shoot():
	shoot_ray.rpc(100)


func compute_ray(distance: float, has_spray = true):
	var c = fps_camera.camera
	var rand_x = randf_range(-1, 1) * SPRAY if has_spray else 1
	var rand_y = randf_range(-1, 1) * SPRAY if has_spray else 1
	var start = c.global_position
	var end = c.global_position - c.global_transform.basis.z * distance
	# Add some random spray to the ray
	end += c.global_transform.basis * Vector3(1, 0, 0) * rand_x
	end += c.global_transform.basis * Vector3(0, 1, 0) * rand_y
	return [start, end]


func shoot_ray(distance: float, has_spray = true):
	var ray = compute_ray(distance, has_spray)
	var start = ray[0]
	var end = ray[1]
	var space = get_world_3d().direct_space_state
	var query = PhysicsRayQueryParameters3D.create(start, end)
	if debug_ray:
		debug_ray.trace(start, end)
	var collision = space.intersect_ray(query)

	if multiplayer.is_server() and collision and collision.collider.is_in_group("Players"):
		var player: FpsCharacter = collision.collider
		player.health -= hit_damage
	return end
