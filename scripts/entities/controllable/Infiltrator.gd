class_name Infiltrator

extends FpsCharacter

signal infiltration_changed(value: bool)
signal data_carrying_changed(value: int)
signal max_data_carrying_changed(value: int)
signal infiltrator_core_carrying_changed(value: Array[int])

@export var infiltrating: bool:
	get:
		return infiltrating
	set(value):
		infiltrating = value
		infiltration_changed.emit(infiltrating)
		if value:
			camera.environment = infiltration_env
		else:
			camera.environment = spawn_env

@export var data_carrying = 0:
	set(value):
		data_carrying = value
		data_carrying_changed.emit(value)

@export var max_data_carrying = 0:
	set(value):
		max_data_carrying = value
		max_data_carrying_changed.emit(value)

@export var infiltrator_core_carrying: Array[int]:
	set(value):
		infiltrator_core_carrying = value
		infiltrator_core_carrying_changed.emit(value)

var teleporting := false

@onready var camera: Camera3D = $FpsCamera/Camera3D
@onready var flashlight = $FpsCamera/Flashlight
@onready var hud = $InfiltratorHUD
@onready var infiltration_env: Environment = preload("res://resources/main-env.tres")
@onready var spawn_env: Environment = preload("res://resources/spawn-env.tres")


func _ready() -> void:
	super()
	infiltrating = false
	max_data_carrying = 250
	infiltrator_core_carrying = []


func _process(delta: float) -> void:
	super(delta)
	if player_input.flashlight:
		flashlight.visible = not flashlight.visible
	player_input.flashlight = false
	_rotate_wheel(delta)
	hud.visible = _is_local_player_controlling() or spectating


func _rotate_wheel(delta):
	var forward = fps_camera.transform.basis * Vector3(0, 0, 1)
	forward.y = 0
	forward = forward.normalized()
	var angle = Vector3(0, 0, 1).signed_angle_to(player_input.direction, Vector3.UP)
	if not player_input.direction.is_zero_approx():
		model.set_wheel_rotation(lerp_angle(model.get_wheel_rotation(), angle, delta * 25))
