extends CanvasLayer

const FADE_OUT_TIMER = 2

var health: int
var sprint_energy: int
var previous_sprint_energy: int
var fade_out_sprint = false
var was_sprinting = false

var data_carrying: int
var old_data_carrying: int
var max_data_carrying: int

var new_total_tween: Tween
var new_carried_tween: Tween
var new_money_tween: Tween

var old_total_data: int
var old_money: int

var look_at_object: LookAtHint

@onready var infiltrating_shaders: Control = $InfiltratingShaders
@onready var health_progress_bar: ProgressBar = %HealthProgressBar
@onready var sprint_progress_bar: ProgressBar = %SprintProgressBar
@onready var data_carrying_label: Label = %DataCarryingLabel
@onready var new_data_carrying_label: Label = %NewDataCarryingLabel
@onready var total_data_label: Label = %TotalDataLabel
@onready var new_total_data_label: Label = %NewTotalDataLabel
@onready var look_at_help_label: Label = %LookAtHelpLabel
@onready var security_progress_label: Label = %SecurityProgressLabel
@onready var money_label: Label = %MoneyLabel
@onready var new_money_label: Label = %NewMoneyLabel
@onready var infiltrator_cores_label: Label = %InfiltratorCoresLabel


func _ready() -> void:
	_update_carried_label()
	_update_total_label()
	_update_money_label()


func _process(delta: float) -> void:
	_update_health_meter(delta)
	if old_data_carrying != data_carrying:
		_play_new_carried_animation(data_carrying - old_data_carrying)
	old_data_carrying = data_carrying
	_update_energy(delta)
	if old_total_data != GlobalGameState.total_uploaded_data:
		_play_new_total_animation(GlobalGameState.total_uploaded_data - old_total_data)
	old_total_data = GlobalGameState.total_uploaded_data

	if old_money != GlobalGameState.infiltrator_money:
		_play_new_money_animation(GlobalGameState.infiltrator_money - old_money)
	old_money = GlobalGameState.infiltrator_money
	# Pickupable objects are freed on pickup
	# Make sure the stored object still exists
	if look_at_object and is_instance_valid(look_at_object):
		look_at_help_label.text = look_at_object.look_at_hint
		look_at_help_label.visible = true
	else:
		look_at_help_label.visible = false
	var security_progress_percent = (
		100
		* (GlobalGameState.security_progress_start - GlobalGameState.security_progress)
		/ GlobalGameState.security_progress_start
	)
	security_progress_label.text = "Security Progress: %s%%" % str(security_progress_percent)


func _play_new_total_animation(added_value: int):
	if new_total_tween:
		new_total_tween.stop()
	var label = new_total_data_label
	label.modulate = Color(255, 255, 255, 1)
	label.text = ("+" if added_value > 0 else "") + str(added_value)
	new_total_tween = create_tween()
	new_total_tween.set_ease(Tween.EASE_OUT)
	new_total_tween.tween_property(label, "position:x", label.position.x + 30, 1)
	new_total_tween.parallel().tween_property(label, "modulate", Color(255, 255, 255, 0), 1)
	# Reset the label position so the container can place it
	new_total_tween.tween_property(label, "position:x", label.position.x, 0)
	new_total_tween.tween_callback(_update_total_label)
	new_total_tween.play()


func _play_new_carried_animation(added_value: int):
	if new_carried_tween:
		new_carried_tween.stop()
	var label = new_data_carrying_label
	label.modulate = Color(255, 255, 255, 1)
	label.text = ("+" if added_value > 0 else "") + str(added_value)
	new_carried_tween = create_tween()
	new_carried_tween.set_ease(Tween.EASE_OUT)
	new_carried_tween.tween_property(label, "position:x", label.position.x + 30, 1)
	new_carried_tween.parallel().tween_property(label, "modulate", Color(255, 255, 255, 0), 1)
	# Reset the label position so the container can place it
	new_carried_tween.tween_property(label, "position:x", label.position.x, 0)
	new_carried_tween.tween_callback(_update_carried_label)
	new_carried_tween.play()


func _play_new_money_animation(added_value: int):
	if new_money_tween:
		new_money_tween.stop()
	var label = new_money_label
	label.modulate = Color(255, 255, 255, 1)
	label.text = ("+" if added_value > 0 else "") + str(added_value)
	new_money_tween = create_tween()
	new_money_tween.set_ease(Tween.EASE_OUT)
	new_money_tween.tween_property(label, "position:x", label.position.x + 30, 1)
	new_money_tween.parallel().tween_property(label, "modulate", Color(255, 255, 255, 0), 1)
	# Reset the label position so the container can place it
	new_money_tween.tween_property(label, "position:x", label.position.x, 0)
	new_money_tween.tween_callback(_update_money_label)
	new_money_tween.play()


func _update_total_label():
	total_data_label.text = (
		"Total: %s/%s"
		% [str(GlobalGameState.total_uploaded_data), str(GlobalGameState.total_uploaded_data_goal)]
	)


func _update_carried_label():
	if data_carrying_label:
		data_carrying_label.text = (
			"Data stolen: " + str(data_carrying) + "/" + str(max_data_carrying)
		)


func _update_money_label():
	money_label.text = ("Shared Money: %s" % str(GlobalGameState.infiltrator_money))


func _update_energy(_delta: float):
	if not fade_out_sprint and sprint_progress_bar.value == 100:
		get_tree().create_timer(FADE_OUT_TIMER).timeout.connect(_on_sprint_fade_timeout)
	elif fade_out_sprint and sprint_progress_bar.value < 100:
		fade_out_sprint = false

	var progress_bar_color = sprint_progress_bar.get_modulate()
	var progress_bar_alpha = progress_bar_color[3]
	if fade_out_sprint and progress_bar_alpha > 0:
		var new_color = lerp(progress_bar_color, Color(0.09, 0.471, 0.761, 0), 0.2)
		if new_color[3] < 0.01:
			new_color[3] = 0
		sprint_progress_bar.set_modulate(new_color)
	elif !fade_out_sprint && progress_bar_alpha < 1:
		var new_color = lerp(progress_bar_color, Color(0.09, 0.471, 0.761, 1), 0.2)
		if new_color[3] > 0.99:
			new_color[3] = 1
		sprint_progress_bar.set_modulate(new_color)
	sprint_progress_bar.value = sprint_energy


func _on_sprint_fade_timeout():
	fade_out_sprint = true


func _update_health_meter(delta: float):
	if health != int(health_progress_bar.value):
		health_progress_bar.value = lerp(health_progress_bar.value, float(health), delta * 10)
		if health_progress_bar.value < 30:
			health_progress_bar.set_modulate(Color(0.655, 0.082, 0.161, 1))
		else:
			health_progress_bar.set_modulate(Color(0.243, 0.78, 0.243, 1))


func _on_infiltrator_health_changed(player: Infiltrator) -> void:
	health = player.health


func _on_infiltrator_sprint_energy_changed(energy: int) -> void:
	sprint_energy = energy


func _on_infiltrator_infiltration_changed(value: bool) -> void:
	infiltrating_shaders.visible = value


func _on_infiltrator_data_carrying_changed(value: int) -> void:
	data_carrying = value


func _on_infiltrator_max_data_carrying_changed(value: int) -> void:
	max_data_carrying = value
	_update_carried_label()


func _on_infiltrator_look_at_object(object: LookAtHint) -> void:
	look_at_object = object


func _on_infiltrator_stop_look_at_object() -> void:
	look_at_object = null


func _on_infiltrator_infiltrator_core_carrying_changed(value: Array[int]) -> void:
	if not infiltrator_cores_label:
		return
	var amount = len(value)
	if amount > 0:
		infiltrator_cores_label.text = "Infiltrator cores: %d" % amount
	else:
		infiltrator_cores_label.text = ""
