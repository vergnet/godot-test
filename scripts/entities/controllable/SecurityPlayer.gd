class_name SecurityPlayer

extends Node3D

signal switch_to(device: SecurityFpsCharacter)
signal place_device(device: SecurityDevicesUtils.Devices)
signal money_changed(value: int)

@export var player_controlled = false
@export var player := 1:
	set(id):
		player = id

@export var security_money: int:
	set(value):
		security_money = value
		money_changed.emit(value)

@export var mouse_sensitivity = 500
@export var max_zoom = 2
@export var min_zoom = 80
@export var zoom_step = 2
@export var zoom_speed := 0.1

@export var security_alert_container: SecurityAlertsContainer

var initial_mouse_position: Vector2
var previous_mouse_position: Vector2
var input_mouse: Vector2
var dragging := false
var target_zoom: int
var tween: Tween

var hovered_device: Node3D
var waiting_click = false
var device_previewed: SecurityDevicesUtils.Devices

@onready var camera = $Camera3D
@onready var security_hud: SecurityHUD = $SecurityHUD


func _ready():
	target_zoom = camera.size
	SignalBus.hover_start.connect(_on_hover_start)
	SignalBus.hover_end.connect(_on_hover_end)


func _process(delta):
	camera.current = player_controlled

	if _is_local_player_controlling():
		Input.mouse_mode = Input.MOUSE_MODE_VISIBLE

	if _is_local_player_controlling():
		_handle_zoom(delta)

	security_hud.visible = _is_local_player_controlling()
	security_hud.clear_alerts()
	for c in security_alert_container.get_children():
		_show_offscreen_alert(c)


func is_placing_device():
	return device_previewed != SecurityDevicesUtils.Devices.NONE


func _show_offscreen_alert(alert: Node3D):
	var camera_pos: Vector2 = camera.unproject_position(alert.global_position)
	var visible_rect = camera.get_viewport().get_visible_rect()
	var padding = Vector2(50, 20)
	var clamped_pos = camera_pos.clamp(visible_rect.position + padding, visible_rect.end - padding)
	if clamped_pos != camera_pos:
		security_hud.add_alert(clamped_pos)


func _is_local_player_controlling():
	return player_controlled and player == multiplayer.get_unique_id()


func _wait_for_click():
	waiting_click = true
	await get_tree().create_timer(0.5).timeout
	waiting_click = false


# Only listen to mouse events not captured by the UI
func _unhandled_input(event: InputEvent) -> void:
	if not _is_local_player_controlling():
		return
	var mouse_pos = get_viewport().get_mouse_position()
	if event.is_action_pressed("shoot"):
		previous_mouse_position = mouse_pos
		initial_mouse_position = mouse_pos
		dragging = true
		_wait_for_click()
	elif event.is_action_released("shoot"):
		dragging = false
		if waiting_click and mouse_pos.distance_to(initial_mouse_position) < 1:
			_handle_click()
	if dragging:
		input_mouse = (mouse_pos - previous_mouse_position) / mouse_sensitivity
		previous_mouse_position = mouse_pos
		position += (
			(transform.basis.x * input_mouse.x + transform.basis.z * input_mouse.y)
			* camera.size
			/ 2
		)


func _handle_zoom(_delta):
	var zoom_in := false
	var zoom_out := false
	if Input.is_action_just_pressed("zoom_in"):
		zoom_in = true
	elif Input.is_action_just_pressed("zoom_out"):
		zoom_out = true

	if zoom_in and target_zoom > max_zoom:
		if target_zoom > 20:
			target_zoom -= zoom_step * 2
		else:
			target_zoom -= zoom_step
	if zoom_out and target_zoom < min_zoom:
		if target_zoom >= 20:
			target_zoom += zoom_step * 2
		else:
			target_zoom += zoom_step
	zoom_to(target_zoom)


func zoom_to(new_value: int):
	if new_value == camera.size:
		return
	if tween:
		tween.stop()

	tween = create_tween()
	tween.tween_property(camera, "size", new_value, zoom_speed)
	tween.play()


func _handle_click():
	if is_placing_device():
		place_device.emit(device_previewed)
	elif hovered_device != null and is_instance_of(hovered_device, SecurityFpsCharacter):
		switch_to.emit(hovered_device)


func _on_hover_start(node: Node3D):
	Input.set_default_cursor_shape(Input.CURSOR_POINTING_HAND)
	hovered_device = node


func _on_hover_end(_node: Node3D):
	Input.set_default_cursor_shape(Input.CURSOR_ARROW)
	hovered_device = null


func _on_security_hud_start_device_placement(device: SecurityDevicesUtils.Devices) -> void:
	device_previewed = device
