extends ShootingFpsCharacter

@onready var hit_particles = $FpsCamera/MeleeHitParticles


func shoot():
	hit_particles.fire()
	shoot_ray.rpc(2, false)
