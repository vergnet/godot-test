extends ShootingFpsCharacter

@onready var muzzle_flash_particles = $FpsCamera/FpsOnly/MuzzleFlashParticles


func _process(delta):
	super(delta)
	model.set_gun_rotation(fps_camera.rotation.x)


@rpc("any_peer", "call_local", "reliable")
func shoot():
	var end = shoot_ray(100)
	model.shoot(end)
	muzzle_flash_particles.fire(end)
