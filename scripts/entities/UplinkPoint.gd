class_name UplinkPoint

extends Node3D

signal teleport(player: Infiltrator)

var player_teleporting: Infiltrator


func _on_area_3d_body_entered(body: Node3D) -> void:
	if body.is_in_group("Players"):
		var player: Infiltrator = body
		if not player.teleporting:
			player_teleporting = player
			player.teleporting = true
			teleport.emit(body)


func _on_area_3d_body_exited(body: Node3D) -> void:
	if body.is_in_group("Players"):
		var player: Infiltrator = body
		if not player_teleporting == player:
			player.teleporting = false
		else:
			player_teleporting = null
