class_name SecurityDevicePreview

extends Node3D

var current_preview: Node3D
var can_place = true
var feedback_mesh_instance_3d_material: Material

var preview_rotation = 0

var device: SecurityDevicesUtils.Devices:
	set(value):
		if value == SecurityDevicesUtils.Devices.NONE:
			reset_preview()
		if value != device and value != SecurityDevicesUtils.Devices.NONE:
			reset_preview()
			var data = SecurityDevicesUtils.get_device_data(value)
			current_preview = load(data.model_path).instantiate()
			add_child(current_preview)
			feedback_mesh_instance_3d.visible = true
		device = value

@onready var room_check_area_3d: Area3D = $RoomCheckArea3D
@onready var feedback_mesh_instance_3d: MeshInstance3D = $FeedbackMeshInstance3D


func _ready() -> void:
	feedback_mesh_instance_3d_material = feedback_mesh_instance_3d.mesh.surface_get_material(0)


func _process(_delta: float) -> void:
	room_check_area_3d.visible = device != SecurityDevicesUtils.Devices.NONE
	if can_place:
		feedback_mesh_instance_3d_material.albedo_color = Color(0, 1, 0, 0.2)
	else:
		feedback_mesh_instance_3d_material.albedo_color = Color(1, 0, 0, 0.2)

	if current_preview:
		if device == SecurityDevicesUtils.Devices.CAMERA:
			# Camera is always on roof
			current_preview.position.y = 3.8
		elif device == SecurityDevicesUtils.Devices.LASER_DETECTOR:
			# Laser is always at head height
			current_preview.position.y = 1.7
		else:
			current_preview.position.y = 0
		current_preview.global_rotation.y = preview_rotation


func _input(event: InputEvent) -> void:
	if event.is_action_pressed("move_right"):
		preview_rotation -= deg_to_rad(45)
	elif event.is_action_pressed("move_left"):
		preview_rotation += deg_to_rad(45)


func reset_preview():
	if current_preview:
		current_preview.queue_free()
		current_preview = null
	feedback_mesh_instance_3d.visible = false


func _is_in_required_group(body: Node3D):
	if device != SecurityDevicesUtils.Devices.LASER_DETECTOR:
		return body.is_in_group("Can Place Device")
	return body.is_in_group("Can Place Lasers")


func _is_in_forbidden_group(body: Node3D):
	return body.is_in_group("Security Devices") or body.is_in_group("World")


func _get_all_overlapping():
	var data = []
	data.append_array(room_check_area_3d.get_overlapping_bodies())
	data.append_array(room_check_area_3d.get_overlapping_areas())
	return data


func get_place_position():
	var final_pos = position
	var final_rotation = rotation
	final_rotation.y = preview_rotation
	if device == SecurityDevicesUtils.Devices.CAMERA:
		# Camera is always on roof
		final_pos.y = 3.8
	if device == SecurityDevicesUtils.Devices.LASER_DETECTOR:
		# lasers are always head height so players can crouch under
		# Span from one wall to the other
		# Can only be placed in corridors
		# Snaps to walls on placement
		var areas = room_check_area_3d.get_overlapping_areas()
		var laser_area: Area3D
		for a in room_check_area_3d.get_overlapping_areas():
			if _is_in_required_group(a):
				laser_area = a
		assert(laser_area != null, "Could not find compatible laser area")
		var collision_shape: CollisionShape3D = laser_area.get_child(0)
		var shape: BoxShape3D = collision_shape.shape
		var rotated = laser_area.global_rotation.y != 0
		var local_final_pos = laser_area.to_local(final_pos)
		if shape.size.x == 4:
			local_final_pos.x = laser_area.position.x
		elif shape.size.y == 4:
			local_final_pos.y = laser_area.position.y
		final_pos = laser_area.to_global(local_final_pos)
		final_pos.y = 1.7
		final_rotation = laser_area.global_rotation
	return [final_pos, final_rotation]


func _check_can_place():
	var allowed = false
	var blocked = false
	for b in _get_all_overlapping():
		if _is_in_forbidden_group(b):
			blocked = true
			break
		if _is_in_required_group(b):
			allowed = true
	return allowed and not blocked


func _on_room_check_area_3d_body_entered(_body: Node3D) -> void:
	can_place = _check_can_place()


func _on_room_check_area_3d_body_exited(_body: Node3D) -> void:
	can_place = _check_can_place()
