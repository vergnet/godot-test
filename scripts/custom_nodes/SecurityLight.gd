extends Node3D

@export var enabled = false:
	set(value):
		enabled = value
		light.visible = value

@onready var light = $OmniLight3D
