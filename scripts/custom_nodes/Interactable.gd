class_name Interactable

extends LookAtHint

signal interacted

@export var can_interact = true


func interact():
	if can_interact:
		interacted.emit()
