extends Camera3D

@onready var security_shaders: Control = $CanvasLayer/SecurityShaders


func _process(_delta):
	if current != security_shaders.visible:
		security_shaders.visible = current
