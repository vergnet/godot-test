class_name SecurityAlertsContainer

extends Node3D

const ALERT = preload("res://scenes/custom_nodes/SecurityAlert.tscn")

# Time an alert stays on screen
const ALERT_TIME = 3


func add_alert(alert_position: Vector3):
	var alert: Node3D = ALERT.instantiate()
	add_child(alert, true)
	alert.global_position = alert_position
	alert.scale = Vector3()
	var tween = create_tween()
	tween.set_trans(Tween.TRANS_ELASTIC)
	tween.tween_property(alert, "scale", Vector3(1.5, 1.5, 1.5), 1)
	tween.play()
	await get_tree().create_timer(ALERT_TIME).timeout
	tween = create_tween()
	tween.set_trans(Tween.TRANS_ELASTIC)
	tween.tween_property(alert, "scale", Vector3(), 1)
	tween.tween_callback(func(): _remove_alert(alert))
	tween.play()


func _remove_alert(alert: Node3D):
	alert.queue_free()
