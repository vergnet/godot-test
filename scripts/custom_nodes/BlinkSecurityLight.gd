class_name BlinkSecurityLight

extends Node3D

@onready var light = $OmniLight3D
@onready var timer = $Timer


func _ready():
	light.visible = false


func blink():
	light.visible = true
	timer.start()


func _on_timer_timeout():
	light.visible = false
