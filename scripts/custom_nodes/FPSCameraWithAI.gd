class_name FpsCameraWithAI

extends FpsCamera

signal player_focused(player: FpsCharacter)
signal player_lost_focus

@export_category("Debug")
@export var show_viewport = false

var focused_player: FpsCharacter

@onready var viewport = $CameraFov
@onready var debug_ray = $RayDebug


func _process(_delta):
	viewport.visible = show_viewport


func _on_camera_fov_player_detected(player):
	player_focused.emit(player)
	focused_player = player


func _on_camera_fov_player_lost(player):
	if focused_player == player:
		player_lost_focus.emit()
		focused_player = null
