class_name RayDebug

extends MeshInstance3D


func trace(start: Vector3, end: Vector3):
	mesh.surface_begin(Mesh.PRIMITIVE_LINE_STRIP)
	mesh.surface_add_vertex(to_local(start))
	mesh.surface_add_vertex(to_local(end))
	mesh.surface_end()
