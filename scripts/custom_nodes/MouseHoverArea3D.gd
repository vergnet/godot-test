extends Area3D

@export var model: Node3D
var outlined := false


func _ready():
	assert(model != null)


func set_outlined(value: bool):
	outlined = value
	Utils.set_node_outline(model, Utils.OutlineType.REGULAR if outlined else Utils.OutlineType.NONE)


func _on_mouse_entered():
	if outlined:
		Utils.set_node_outline(model, Utils.OutlineType.HIGHLIGHT)
		SignalBus.hover_start.emit(get_parent())


func _on_mouse_exited():
	if outlined:
		Utils.set_node_outline(model, Utils.OutlineType.REGULAR)
		SignalBus.hover_end.emit(get_parent())
