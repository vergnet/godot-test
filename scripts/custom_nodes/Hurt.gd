extends Node3D

@export var damage = 10


func _on_area_3d_body_entered(body: Node3D):
	if multiplayer.is_server() and body.is_in_group("Players"):
		var infiltrator: Infiltrator = body
		infiltrator.health -= damage
