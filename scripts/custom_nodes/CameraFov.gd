extends Node3D

signal player_detected(player: FpsCharacter)
signal player_lost(player: FpsCharacter)

var players_in_area: Array[FpsCharacter] = []
var players_detected: Array[FpsCharacter] = []

var can_notify = true

@onready var polling_timer: Timer = $PollingTimer
@onready var notify_timer: Timer = $NotifyTimer


func _on_area_3d_body_entered(body):
	if body.is_in_group("Players"):
		if not body in players_in_area:
			players_in_area.append(body)


func _on_area_3d_body_exited(body):
	if body.is_in_group("Players"):
		players_in_area.erase(body)
		if body in players_detected:
			_lose_player(body)


func _on_polling_timer_timeout():
	for player in players_in_area:
		shoot_ray(player)


func shoot_ray(player: FpsCharacter):
	var space = get_world_3d().direct_space_state
	var query = PhysicsRayQueryParameters3D.create(global_position, player.global_position)
	var collision = space.intersect_ray(query)
	if collision and collision.collider == player:
		if not player in players_detected:
			_find_player(player)
	else:
		_lose_player(player)


@rpc("any_peer", "call_local", "reliable")
func notify_position(player_position: Vector3):
	SignalBus.infiltrator_detected.emit(player_position)


func _process(_delta: float) -> void:
	if can_notify and len(players_detected) > 0:
		notify_timer.start()
		can_notify = false
		notify_position.rpc()
		for p in players_detected:
			notify_position.rpc(p.global_position)


func _find_player(player: FpsCharacter):
	players_detected.append(player)
	player_detected.emit(player)
	notify_position.rpc(player.global_position)


func _lose_player(player: FpsCharacter):
	players_detected.erase(player)
	player_lost.emit(player)


func _on_notify_timer_timeout() -> void:
	can_notify = true
