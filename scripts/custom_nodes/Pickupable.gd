class_name Pickupable

extends Interactable

signal picked_up(player: Infiltrator)


func _on_body_entered(body: Node3D) -> void:
	# Make sure only the server handles pickup events
	if body.is_in_group("Players") and multiplayer.is_server():
		picked_up.emit(body)
