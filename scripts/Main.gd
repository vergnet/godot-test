extends Node

var demo_scene = preload("res://scenes/Level.tscn")
@onready var level: Node3D = $Level
@onready var main_menu: MainMenu = $MainMenu


func _ready() -> void:
	SteamLobby.on_lobby_join.connect(_on_lobby_join)


func _on_lobby_join():
	main_menu.queue_free()
	main_menu = null
	if multiplayer.is_server():
		change_level.call_deferred(demo_scene)


func change_level(scene: PackedScene):
	# Remove old level if any.
	for c in level.get_children():
		level.remove_child(c)
		c.queue_free()
	# Add new level.
	var instance: Level = scene.instantiate()
	instance.win_infiltrators.connect(_on_win_infiltrators)
	instance.win_security.connect(_on_win_security)
	level.add_child(instance)


func _reset_game():
	pass


func _on_win_infiltrators():
	print("Quota reached, Infiltrators win")
	_reset_game()


func _on_win_security(reason: String):
	print("%s, Security wins" % reason)
	_reset_game()
