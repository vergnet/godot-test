extends Node3D

@onready var muzzle_flash_particles = $MuzzleFlashParticles
@onready var tracer_particles = $BulletTracerParticles
@onready var light = $OmniLight3D
@onready var timer = $LightTimer


func _ready():
	light.visible = false


func fire(target: Vector3):
	tracer_particles.look_at(target, Vector3(0, 1, 0), true)
	tracer_particles.emitting = true
	muzzle_flash_particles.emitting = true
	light.visible = true
	timer.start()


func _on_light_timer_timeout():
	light.visible = false
