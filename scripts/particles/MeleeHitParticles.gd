extends Node3D

@onready var particles = $MeleeHitParticles
@onready var light = $OmniLight3D
@onready var timer = $LightTimer


func _ready():
	light.visible = false


func fire():
	particles.emitting = true
	light.visible = true
	timer.start()


func _on_light_timer_timeout():
	light.visible = false
