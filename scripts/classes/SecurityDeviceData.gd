class_name SecurityDeviceData

var device_name: String
var price: int
var scene_path: String
var model_path: String


func _init(
	this_device_name: String, this_price: int, this_scene_path: String, this_model_path: String
):
	device_name = this_device_name
	price = this_price
	scene_path = this_scene_path
	assert(ResourceLoader.exists(scene_path), "Invalid scene path for device %s" % device_name)
	model_path = this_model_path
	assert(ResourceLoader.exists(model_path), "Invalid model path for device %s" % device_name)
