extends Node

enum OutlineType { REGULAR, HIGHLIGHT, NONE }

var _outline_shader: Shader = load("res://shaders/outline.gdshader")


func set_model_fps_invisible(model: MeshInstance3D, invisible: bool):
	model.set_layer_mask_value(1, !invisible)
	model.set_layer_mask_value(2, invisible)


func set_model_children_fps_invisible(model: MeshInstance3D, controlled: bool):
	set_model_fps_invisible(model, controlled)
	for child in model.get_children():
		if is_instance_of(child, MeshInstance3D):
			set_model_fps_invisible(child, controlled)


func outline_mesh(mesh: Mesh, color: Color, border_width: float):
	var material = mesh.surface_get_material(0).duplicate()
	var next_pass = ShaderMaterial.new()
	next_pass.shader = _outline_shader
	next_pass.set_shader_parameter("color", color)
	next_pass.set_shader_parameter("border_width", border_width)
	material.set_next_pass(next_pass)
	mesh.surface_set_material(0, material)


func outline_node(node: Node3D, color: Color = Color.RED, border_width: float = 0.2):
	for c in node.get_children():
		if is_instance_of(c, MeshInstance3D):
			# make sure the outline gets applied to individual instances
			var new_mesh = c.mesh.duplicate()
			outline_mesh(new_mesh, color, border_width)
			c.mesh = new_mesh
			outline_node(c, color, border_width)
		elif is_instance_of(c, Node3D):
			outline_node(c, color, border_width)


func remove_mesh_outline(mesh: Mesh):
	var material = mesh.surface_get_material(0)
	material.set_next_pass(null)


func remove_node_outline(node: Node3D):
	for c in node.get_children():
		if is_instance_of(c, MeshInstance3D):
			remove_mesh_outline(c.mesh)
			remove_node_outline(c)
		elif is_instance_of(c, Node3D):
			remove_node_outline(c)


func set_node_outline(node: Node3D, type: OutlineType):
	match type:
		OutlineType.REGULAR:
			outline_node(node)
		OutlineType.HIGHLIGHT:
			outline_node(node, Color.DARK_GREEN, 0.3)
		OutlineType.NONE:
			remove_node_outline(node)
