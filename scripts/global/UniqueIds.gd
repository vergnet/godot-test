extends Node

var _last_security_device_id = 0


func get_security_device_id():
	_last_security_device_id += 1
	return _last_security_device_id
