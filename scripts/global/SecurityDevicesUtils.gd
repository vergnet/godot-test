extends Node

enum Devices { NONE, TURRET, RANGE_ROBOT, MELEE_ROBOT, CAMERA, SCOUT_ROBOT, MINE, LASER_DETECTOR }

var _devices_dict: Dictionary = {
	Devices.TURRET:
	SecurityDeviceData.new(
		"Turret",
		10,
		"res://scenes/entities/controllable/SecurityTurret.tscn",
		"res://scenes/models/TurretModel.tscn"
	),
	Devices.RANGE_ROBOT:
	SecurityDeviceData.new(
		"Range Robot",
		200,
		"res://scenes/entities/controllable/SecurityRangeRobot.tscn",
		"res://scenes/models/SecurityRangeRobotModel.tscn"
	),
	Devices.MELEE_ROBOT:
	SecurityDeviceData.new(
		"Melee Robot",
		200,
		"res://scenes/entities/controllable/SecurityMeleeRobot.tscn",
		"res://scenes/models/SecurityMeleeRobotModel.tscn"
	),
	Devices.CAMERA:
	SecurityDeviceData.new(
		"Camera",
		200,
		"res://scenes/entities/controllable/SecurityCamera.tscn",
		"res://scenes/models/SecurityCameraModel.tscn"
	),
	Devices.SCOUT_ROBOT:
	SecurityDeviceData.new(
		"Scout Robot",
		200,
		"res://scenes/entities/controllable/SecurityScout.tscn",
		"res://scenes/models/SecurityScoutModel.tscn"
	),
	Devices.MINE:
	SecurityDeviceData.new(
		"Mine", 200, "res://scenes/entities/SecurityMine.tscn", "res://models/mine.blend"
	),
	Devices.LASER_DETECTOR:
	SecurityDeviceData.new(
		"Laser Detector",
		200,
		"res://scenes/entities/SecurityLaserDetector.tscn",
		"res://models/laser-detector.blend"
	)
}


func get_device_data(device: Devices) -> SecurityDeviceData:
	return _devices_dict[device]


func _get_device_scene(device: Devices):
	var device_data: SecurityDeviceData = _devices_dict[device]
	return load(device_data.scene_path)
