extends Node

# Spacewar ID used for dev
const APP_ID = 480

var steam_username: String = ""
var steam_id: int = 0

var steam_available: bool


func _ready() -> void:
	initialize_steam()


func _process(_delta: float) -> void:
	if steam_available:
		Steam.run_callbacks()


func initialize_steam() -> void:
	var initialize_response: Dictionary = Steam.steamInitEx(true, APP_ID)
	print("Did Steam initialize?: %s " % initialize_response)

	steam_available = initialize_response["status"] == 0
	if steam_available:
		steam_username = Steam.getPersonaName()
		steam_id = Steam.getSteamID()
	else:
		print("Failed to initialize Steam, starting in LAN only: %s" % initialize_response)
