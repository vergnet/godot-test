extends Node


func _quit():
	get_tree().root.propagate_notification(NOTIFICATION_WM_CLOSE_REQUEST)
	get_tree().quit()


func _ready() -> void:
	pass
	#if not OS.has_feature("template"):
	#_switch_windowed()


func _process(_delta):
	if Input.is_action_just_pressed("ui_cancel"):
		_quit()
	elif Input.is_action_just_pressed("ui_text_completion_accept"):
		var current_mode = DisplayServer.window_get_mode()
		if current_mode == DisplayServer.WINDOW_MODE_EXCLUSIVE_FULLSCREEN:
			_switch_windowed()
		else:
			_switch_fullscreen()


func _switch_fullscreen():
	DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_EXCLUSIVE_FULLSCREEN)


func _switch_windowed():
	DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_WINDOWED)
	DisplayServer.window_set_size(Vector2(1152, 648))
