extends Node

const PACKET_READ_LIMIT: int = 32


func _ready() -> void:
	Steam.p2p_session_connect_fail.connect(_on_p2p_session_connect_fail)
	Steam.p2p_session_request.connect(_on_p2p_session_request)


func _process(_delta):
	if SteamLobby.lobby_id > 0:
		read_all_p2p_packets()


func read_all_p2p_packets(read_count: int = 0):
	if read_count >= PACKET_READ_LIMIT:
		return

	if Steam.getAvailableP2PPacketSize(0) > 0:
		read_p2p_packet()
		read_all_p2p_packets(read_count + 1)


func read_p2p_packet() -> void:
	var packet_size: int = Steam.getAvailableP2PPacketSize(0)

	# There is a packet
	if packet_size > 0:
		var this_packet: Dictionary = Steam.readP2PPacket(packet_size, 0)

		if this_packet.is_empty() or this_packet == null:
			print("WARNING: read an empty packet with non-zero size!")

		# Get the remote user's ID
		var packet_sender: int = this_packet["remote_steam_id"]

		# Make the packet data readable
		var packet_code: PackedByteArray = this_packet["data"]

		# Decompress the array before turning it into a useable dictionary
		var readable_data: Dictionary = bytes_to_var(
			packet_code.decompress_dynamic(-1, FileAccess.COMPRESSION_GZIP)
		)

		# Print the packet to output
		print("Packet: %s" % readable_data)

		# Append logic here to deal with packet data


func make_p2p_handshake() -> void:
	print("Sending P2P handshake to the lobby")
	send_p2p_packet(0, {"message": "handshake", "from": GlobalSteam.steam_id})


func _on_p2p_session_request(remote_id: int) -> void:
	# Get the requester's name
	var this_requester: String = Steam.getFriendPersonaName(remote_id)
	print("%s is requesting a P2P session" % this_requester)

	# Accept the P2P session; can apply logic to deny this request if needed
	Steam.acceptP2PSessionWithUser(remote_id)

	# Make the initial handshake
	make_p2p_handshake()


func send_p2p_packet(target: int, packet_data: Dictionary) -> void:
	# Set the send_type and channel
	var send_type: int = Steam.P2P_SEND_RELIABLE
	var channel: int = 0

	# Create a data array to send the data through
	var this_data: PackedByteArray

	# Compress the PackedByteArray we create from our dictionary  using the GZIP compression method
	var compressed_data: PackedByteArray = var_to_bytes(packet_data).compress(
		FileAccess.COMPRESSION_GZIP
	)
	this_data.append_array(compressed_data)

	# If sending a packet to everyone
	if target == 0:
		# If there is more than one user, send packets
		if SteamLobby.lobby_members.size() > 1:
			# Loop through all members that aren't you
			for this_member in SteamLobby.lobby_members:
				if this_member["steam_id"] != GlobalSteam.steam_id:
					Steam.sendP2PPacket(this_member["steam_id"], this_data, send_type, channel)

	# Else send it to someone specific
	else:
		Steam.sendP2PPacket(target, this_data, send_type, channel)


func _on_p2p_session_connect_fail(steam_id: int, session_error: int) -> void:
	# If no error was given
	if session_error == 0:
		print("WARNING: Session failure with %s: no error given" % steam_id)

	# Else if target user was not running the same game
	elif session_error == 1:
		print("WARNING: Session failure with %s: target user not running the same game" % steam_id)

	# Else if local user doesn't own app / game
	elif session_error == 2:
		print("WARNING: Session failure with %s: local user doesn't own app / game" % steam_id)

	# Else if target user isn't connected to Steam
	elif session_error == 3:
		print("WARNING: Session failure with %s: target user isn't connected to Steam" % steam_id)

	# Else if connection timed out
	elif session_error == 4:
		print("WARNING: Session failure with %s: connection timed out" % steam_id)

	# Else if unused
	elif session_error == 5:
		print("WARNING: Session failure with %s: unused" % steam_id)

	# Else no known error
	else:
		print("WARNING: Session failure with %s: unknown error %s" % [steam_id, session_error])
