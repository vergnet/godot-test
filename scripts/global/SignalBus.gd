extends Node

signal hover_start(node: Node3D)
signal hover_end(node: Node3D)

signal infiltrator_detected(infiltrator_position: Vector3)
