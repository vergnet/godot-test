class_name PlayerInput

extends MultiplayerSynchronizer

@export var fps_camera: Node3D

@export var crouching: bool
@export var sprinting: bool
@export var interacting: bool
@export var direction: Vector3
@export var rotation_target: Vector3
@export var flashlight: bool

@export var mouse_sensitivity = 700
@export var min_clamp_x: float = -90
@export var max_clamp_x: float = 90

var enabled = false


func _ready() -> void:
	NetworkTime.before_tick_loop.connect(_gather)
	set_process(_can_capture())


func _process(_delta: float) -> void:
	if not _can_capture():
		return

	if Input.is_action_just_pressed("interact"):
		interacting = true

	if Input.is_action_just_pressed("flashlight"):
		flashlight = true

	crouching = Input.is_action_pressed("crouch")
	sprinting = Input.is_action_pressed("sprint")


func _gather() -> void:
	if not _can_capture():
		return
	direction = _get_direction()


func _input(event):
	if _can_capture() and event is InputEventMouseMotion:
		var input_mouse = event.relative / mouse_sensitivity
		rotation_target.y -= input_mouse.x
		rotation_target.x = _get_clamped_rotation_x(rotation_target.x + input_mouse.y)


func _can_capture():
	return enabled and is_multiplayer_authority()


func _get_clamped_rotation_x(target: float):
	return clamp(target, deg_to_rad(min_clamp_x), deg_to_rad(max_clamp_x))


func _get_direction():
	var input_dir = Input.get_vector("move_left", "move_right", "move_forward", "move_back")
	var new_direction = fps_camera.transform.basis * Vector3(-input_dir.x, 0, -input_dir.y)
	new_direction.y = 0
	new_direction = new_direction.normalized()
	return new_direction
