extends PlayableModel

@onready var muzzle_flash_particles = $"Rotor base/Gun base/Barrel/MuzzleFlashParticles"


func shoot(target: Vector3):
	muzzle_flash_particles.fire(target)
