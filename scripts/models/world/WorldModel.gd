class_name WorldModel

extends Node3D

@onready var roof: Node3D = $"Walls Top"
@onready var floor: Node3D = $"Walls Bottom"


func _ready() -> void:
	_add_to_group(floor)


func _add_to_group(node: Node3D):
	node.add_to_group("World")
	for n in node.get_children():
		n.add_to_group("World")
		if n.get_child_count() > 0:
			_add_to_group(n)


func set_roof_visible(value: bool):
	roof.visible = value
