extends Node3D

signal door_closed
signal door_opened

const OPEN_DISTANCE = 1.2
const OPEN_TIME = 0.7

var tween_door: Tween
var _initial_door1_position
var _initial_door2_position

@onready var door1 = $"Door 1"
@onready var door2 = $"Door 2"


func _ready():
	_initial_door1_position = door1.position.x
	_initial_door2_position = door2.position.x
	_add_to_group(self)


func _add_to_group(node: Node3D):
	node.add_to_group("World")
	for n in node.get_children():
		n.add_to_group("World")
		if n.get_child_count() > 0:
			_add_to_group(n)


func move_door_animation(open: bool):
	if tween_door:
		tween_door.kill()

	tween_door = create_tween()
	tween_door.set_ease(Tween.EASE_OUT)
	tween_door.set_trans(Tween.TRANS_EXPO)
	tween_door.tween_property(
		door1,
		"position:x",
		_initial_door1_position - OPEN_DISTANCE if open else _initial_door1_position,
		OPEN_TIME
	)
	tween_door.parallel().tween_property(
		door2,
		"position:x",
		_initial_door2_position + OPEN_DISTANCE if open else _initial_door2_position,
		OPEN_TIME
	)
	tween_door.tween_callback(door_opened.emit if open else door_closed.emit)


func open():
	if not tween_door or not tween_door.is_running():
		move_door_animation(true)


func close():
	if not tween_door or not tween_door.is_running():
		move_door_animation(false)
