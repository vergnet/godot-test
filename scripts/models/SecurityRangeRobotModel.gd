extends PlayableModel

@onready var gun = $"Body/Gun base"
@onready var muzzle_flash_particles = $"Body/Gun base/Barrel/MuzzleFlashParticles"


func get_gun_roation():
	return gun.rotation.x


func set_gun_rotation(r: float):
	gun.rotation.x = r


func shoot(target: Vector3):
	muzzle_flash_particles.fire(target)
