extends PlayableModel

const CROUCH_OFFSET = 0.4

var crouched = false
var initial_body_height

@onready var wheel = $Wheel
@onready var body = $Body


func _ready():
	initial_body_height = body.position.y


func _process(delta):
	var new_body_height = initial_body_height
	if crouched:
		new_body_height = initial_body_height - CROUCH_OFFSET

	if body.position.y != new_body_height:
		body.position.y = lerp(body.position.y, new_body_height, delta * 10)


func get_wheel_rotation():
	return wheel.rotation.y


func set_wheel_rotation(r: float):
	wheel.rotation.y = r
