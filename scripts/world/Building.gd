class_name Building

extends Node


func set_roof_visible(value: bool):
	for c in get_children():
		if c.has_method("set_roof_visible"):
			c.set_roof_visible(value)
