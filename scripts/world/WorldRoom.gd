class_name WorldRoom

extends Node3D


func set_roof_visible(value: bool):
	for c in get_children():
		if "set_roof_visible" in c:
			c.set_roof_visible(value)
