extends Node3D

@export var moving = false
@export var open = false:
	set(value):
		if not moving:
			open = value
			moving = true
			if open:
				door.open()
			else:
				door.close()

@onready var door = $door
@onready var interactable: Interactable = $Interactable

@rpc("any_peer", "call_local", "reliable")
func toggle():
	SignalBus.infiltrator_detected.emit(global_position)
	open = not open


func _process(_delta: float) -> void:
	if not moving:
		if open:
			interactable.look_at_hint = "Close [E]"
		else:
			interactable.look_at_hint = "Open [E]"
	else:
		interactable.look_at_hint = ""


func _on_interactable_interacted() -> void:
	toggle.rpc()


func _on_door_door_closed() -> void:
	moving = false


func _on_door_door_opened() -> void:
	moving = false
