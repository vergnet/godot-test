class_name Level

extends Node3D

signal win_security(reason: String)
signal win_infiltrators

const DATA_SHARD = preload("res://scenes/entities/pickups/DataShard.tscn")
const INFILTRATOR = preload("res://scenes/entities/controllable/Infiltrator.tscn")
const DEFAULT_SPAWN = Vector3(-88.783, 1, -3)

@export var infiltrator_money: int:
	set(value):
		infiltrator_money = value
		GlobalGameState.infiltrator_money = value
@export var total_uploaded_data_goal: int:
	set(value):
		total_uploaded_data_goal = value
		GlobalGameState.total_uploaded_data_goal = value
@export var total_uploaded_data: int:
	set(value):
		total_uploaded_data = value
		GlobalGameState.total_uploaded_data = value

@export var security_progress_start: int:
	set(value):
		security_progress_start = value
		GlobalGameState.security_progress_start = value
@export var security_progress: int:
	set(value):
		security_progress = value
		GlobalGameState.security_progress = value

@onready var players: Node3D = $Infiltrators
@onready var security_player: SecurityPlayer = $SecurityPlayer
@onready var security_player_light: DirectionalLight3D = $SecurityPlayerLight
@onready var building: Building = $Building
@onready var security_devices: Node3D = $SecurityDevices
@onready var data_terminals: Node3D = $DataTerminals
@onready var upload_terminal: UploadTerminal = %UploadTerminal
@onready var pickups: Node3D = $Pickups
@onready var security_progress_timer: Timer = $SecurityProgressTimer
@onready var infiltrator_spawn: Node3D = $InfiltratorSpawn
@onready var uplink_points_controller: Node3D = $UplinkPointsController
@onready var security_alerts_container: SecurityAlertsContainer = $SecurityAlertsContainer
@onready var security_device_preview: SecurityDevicePreview = $SecurityDevicePreview
@onready
var security_devices_multiplayer_spawner: MultiplayerSpawner = $SecurityDevicesMultiplayerSpawner
@onready var healing_terminal: HealingTerminal = %HealingTerminal
@onready var respawn_terminal: RespawnTerminal = %RespawnTerminal


func _ready() -> void:
	infiltrator_money = 0
	total_uploaded_data = 0
	total_uploaded_data_goal = 500
	security_player.switch_to.connect(_on_security_switch_to)
	SignalBus.infiltrator_detected.connect(_on_infiltrator_detected)

	for d in SecurityDevicesUtils.Devices.values():
		if d != SecurityDevicesUtils.Devices.NONE:
			var data = SecurityDevicesUtils.get_device_data(d)
			security_devices_multiplayer_spawner.add_spawnable_scene(data.scene_path)

	security_devices_multiplayer_spawner.spawned.connect(_on_device_spawned)

	for c: DataTerminal in data_terminals.get_children():
		c.steal_data.connect(_on_steal_data)

	upload_terminal.upload_data.connect(_on_upload_data)
	healing_terminal.request_heal.connect(_on_request_heal)
	respawn_terminal.request_respawn.connect(_on_request_respawn)

	if not multiplayer.is_server():
		return

	multiplayer.peer_connected.connect(_on_peer_connected)
	multiplayer.peer_disconnected.connect(del_player)

	security_player_light.visible = false
	add_player(multiplayer.get_unique_id(), DEFAULT_SPAWN)
	# Spawn already connected players.
	for id in multiplayer.get_peers():
		add_player(id, DEFAULT_SPAWN)

	security_progress_start = 1000
	security_progress = security_progress_start
	security_progress_timer.timeout.connect(_on_security_progress_timer_tick)
	security_progress_timer.start()


func _on_peer_connected(id: int):
	add_player(id, DEFAULT_SPAWN)


func _on_device_spawned(node: Node):
	if security_player.player == multiplayer.get_unique_id():
		node.set_outlined(true)


func _process(_delta: float) -> void:
	if Input.is_action_just_pressed("switch_camera"):
		switch_security.rpc()
	if security_player.player == multiplayer.get_unique_id():
		security_device_preview.device = security_player.device_previewed
		if security_player.device_previewed != SecurityDevicesUtils.Devices.NONE:
			var mouse_position_3d = _get_mouse_3d_pos()
			security_device_preview.global_position = mouse_position_3d


func _get_mouse_3d_pos() -> Vector3:
	var viewport = get_viewport()
	var mouse_position = viewport.get_mouse_position()
	var camera = viewport.get_camera_3d()
	var origin := camera.project_ray_origin(mouse_position)
	var direction := camera.project_ray_normal(mouse_position)
	var ray_length = camera.far
	var end = origin + direction * ray_length
	var space_state := get_world_3d().direct_space_state
	var query := (
		PhysicsRayQueryParameters3D
		. create(
			origin,
			end,
			# Only layer 4
			8
		)
	)
	var result := space_state.intersect_ray(query)
	var pos = result.get("position", end)
	pos.y = 0
	return pos


func _on_infiltrator_detected(infiltrator_position: Vector3):
	if security_player.player == multiplayer.get_unique_id():
		security_alerts_container.add_alert(infiltrator_position)


func _set_security_visiblity(is_security: bool):
	infiltrator_spawn.visible = not is_security
	uplink_points_controller.visible = not is_security
	players.visible = not is_security


func _on_security_progress_timer_tick():
	var intact_terminals = 0
	for t: DataTerminal in data_terminals.get_children():
		if t.available_data > 0:
			intact_terminals += 1
	security_progress -= 10 + 10 * intact_terminals
	security_player.security_money += 1 + 1 * intact_terminals
	if security_progress <= 0:
		security_progress = 0
		security_progress_timer.stop()
		win_security.emit("Security task finished")


func _on_infiltrator_health_changed(infiltrator: Infiltrator):
	if infiltrator.health <= 0 and multiplayer.is_server():
		print("dead")
		on_player_death.rpc(infiltrator.player)
		_kill_infiltrator(infiltrator.player)


@rpc("authority", "call_local", "reliable")
func on_player_death(player: int):
	var players_alive = false
	for c: Infiltrator in players.get_children():
		if c.player != player:
			c.spectating = player == multiplayer.get_unique_id()
			players_alive = true
			break
	if not players_alive:
		win_security.emit("All infiltrators are out of action")


func _kill_infiltrator(player: int):
	var infiltrator = _get_player(player)
	var death_position = infiltrator.global_position
	var carried_data = infiltrator.data_carrying
	infiltrator.queue_free()
	var data_shard: DataShard = DATA_SHARD.instantiate()
	death_position.y -= 1
	data_shard.global_position = death_position
	var new_array: Array[int] = [player]
	new_array.append_array(infiltrator.infiltrator_core_carrying)
	data_shard.players_contained = new_array
	data_shard.available_data = carried_data
	data_shard.pickup_data.connect(_on_pickup_data)
	pickups.add_child(data_shard, true)


func _on_pickup_data(player: int, data_shard: DataShard):
	var infiltrator = _get_player(player)
	var capacity = infiltrator.max_data_carrying - infiltrator.data_carrying
	var amout_to_pickup = (
		data_shard.available_data if capacity >= data_shard.available_data else capacity
	)
	data_shard.available_data -= amout_to_pickup
	infiltrator.data_carrying += amout_to_pickup
	var new_array: Array[int] = infiltrator.infiltrator_core_carrying.duplicate()
	new_array.append_array(data_shard.players_contained)
	data_shard.players_contained = []
	if data_shard.available_data == 0 and len(data_shard.players_contained) == 0:
		data_shard.queue_free()
	infiltrator.infiltrator_core_carrying = new_array


func _on_steal_data(player: int, terminal: DataTerminal):
	var infiltrator = _get_player(player)
	var capacity = infiltrator.max_data_carrying - infiltrator.data_carrying
	var amout_to_steal = (
		terminal.available_data if capacity >= terminal.available_data else capacity
	)
	terminal.available_data -= amout_to_steal
	infiltrator.data_carrying += amout_to_steal


func _on_upload_data(player: int):
	var infiltrator = _get_player(player)
	total_uploaded_data += infiltrator.data_carrying
	infiltrator_money += infiltrator.data_carrying
	infiltrator.data_carrying = 0
	if total_uploaded_data >= total_uploaded_data_goal:
		win_infiltrators.emit()


func _on_request_respawn(player: int, terminal: RespawnTerminal):
	var infiltrator = _get_player(player)
	var amount_to_respawn = len(infiltrator.infiltrator_core_carrying)
	if amount_to_respawn == 0:
		return

	var price = amount_to_respawn * terminal.price
	if infiltrator_money >= price:
		infiltrator_money -= price
		for p in infiltrator.infiltrator_core_carrying:
			var pos = terminal.global_position
			pos.y = 1
			add_player(p, pos, terminal.global_rotation)
			_on_player_respawn.rpc(p)
		var new_array: Array[int] = []
		infiltrator.infiltrator_core_carrying = new_array


@rpc("authority", "call_local", "reliable")
func _on_player_respawn(player: int):
	if player == multiplayer.get_unique_id():
		for c: Infiltrator in players.get_children():
			c.spectating = false


func _on_request_heal(player: int, terminal: HealingTerminal):
	var infiltrator = _get_player(player)
	if not infiltrator.health < infiltrator.max_health:
		return

	if infiltrator_money >= terminal.price:
		infiltrator_money -= terminal.price
		infiltrator.health += terminal.heal_amount


func _get_player(player: int):
	for c: Infiltrator in players.get_children():
		if c.player == player:
			return c


func _get_device(player: int):
	for c in security_devices.get_children():
		if is_instance_of(c, SecurityFpsCharacter) and c.player_controlled and c.player == player:
			return c


func _get_device_of_id(id: int):
	for c in security_devices.get_children():
		if is_instance_of(c, SecurityFpsCharacter) and c.id == id:
			return c


func _set_security_player_enabled(enabled: bool, player: int):
	security_player.player_controlled = enabled
	security_player.player = player if enabled else 0
	_set_security_visiblity(enabled and player == multiplayer.get_unique_id())
	if player == multiplayer.get_unique_id():
		building.set_roof_visible(not enabled)
		security_player_light.visible = enabled
		for c in security_devices.get_children():
			c.set_outlined(enabled)


@rpc("any_peer", "call_local", "reliable")
func switch_security():
	var player = multiplayer.get_remote_sender_id()
	if _can_switch_to_security(player):
		_switch_security(player)
	else:
		print("Someone is already controlling security")


func _switch_security(player: int):
	var current_player = _get_player(player)
	if current_player.player_controlled or security_player.player_controlled:
		var new_controlled = not current_player.player_controlled
		current_player.player_controlled = new_controlled
		_set_security_player_enabled(not new_controlled, player)
	# we are controlling a device, go back to security
	else:
		_set_security_player_enabled(true, player)
		var current_device = _get_device(player)
		current_device.player_controlled = false
		current_device.player = 0


func _can_switch_to_security(player: int):
	var can_switch = not security_player.player_controlled or security_player.player == player
	if can_switch:
		for c in security_devices.get_children():
			if is_instance_of(c, SecurityFpsCharacter):
				can_switch = not c.player_controlled or c.player == player
				if not can_switch:
					return can_switch
	return can_switch


@rpc("any_peer", "call_local", "reliable")
func switch_device(id: int):
	var device = _get_device_of_id(id)
	var player = multiplayer.get_remote_sender_id()
	_set_security_player_enabled(false, player)
	device.player = player
	device.player_controlled = true


func _exit_tree():
	if not multiplayer.is_server():
		return
	multiplayer.peer_connected.disconnect(_on_peer_connected)
	multiplayer.peer_disconnected.disconnect(del_player)


func add_player(id: int, pos: Vector3, rot: Vector3 = Vector3()):
	var character: Infiltrator = INFILTRATOR.instantiate()
	character.position = pos
	print(rot)
	print(rad_to_deg(rot.y))
	character.player_input.rotation_target.y = rot.y
	character.name = str(id)
	character.player_controlled = true
	character.health_changed.connect(_on_infiltrator_health_changed)
	print("adding player")
	players.add_child(character, true)
	character.player = id
	return character


func del_player(id: int):
	if not players.has_node(str(id)):
		return
	players.get_node(str(id)).queue_free()


func _on_security_switch_to(device: SecurityFpsCharacter):
	switch_device.rpc(device.id)


@rpc("any_peer", "call_local", "reliable")
func place_new_security_device(device: SecurityDevicesUtils.Devices, pos: Vector3, rot: Vector3):
	if (
		multiplayer.is_server()
		and security_device_preview.can_place
		and not device == SecurityDevicesUtils.Devices.NONE
	):
		var device_data = SecurityDevicesUtils.get_device_data(device)
		var price = device_data.price
		if security_player.security_money >= price:
			security_player.security_money -= price
			reset_device_placement.rpc()
			var scene = load(device_data.scene_path).instantiate()
			if is_instance_of(scene, FpsCharacter):
				scene.player_input.rotation_target.y = rot.y
			scene.global_position = pos
			security_devices.add_child(scene, true)
			if not is_instance_of(scene, FpsCharacter):
				# For some reason this needs to be called after the scene is added
				scene.global_rotation = rot
			if security_player.player == multiplayer.get_unique_id():
				scene.set_outlined(true)
		else:
			print("Not enough money to buy")


@rpc("authority", "call_local", "reliable")
func reset_device_placement():
	security_player.device_previewed = SecurityDevicesUtils.Devices.NONE


func _on_security_player_place_device(device: SecurityDevicesUtils.Devices) -> void:
	var p = security_device_preview.get_place_position()
	place_new_security_device.rpc(device, p[0], p[1])
