class_name InfiltratorVitalsContainer

extends VBoxContainer

var player: int

var infiltrator: Infiltrator:
	set(value):
		infiltrator = value
		if infiltrator:
			infiltrator_name_label.text = str(player)
			_set_health_label(infiltrator)
			infiltrator.health_changed.connect(_set_health_label)
			_set_data_carried_label(infiltrator.data_carrying)
			infiltrator.data_carrying_changed.connect(_set_data_carried_label)
		else:
			health_label.text = ""
			data_carried_label.text = ""

@onready var infiltrator_name_label: Label = $InfiltratorNameLabel
@onready var health_label: Label = $HealthLabel
@onready var data_carried_label: Label = $DataCarriedLabel


func _set_health_label(infiltrator: Infiltrator):
	health_label.text = "Health: %d" % infiltrator.health


func _set_data_carried_label(value: int):
	data_carried_label.text = "Data Carried: %d" % value
