class_name SecurityDeviceListItem

extends Button

var device_name: String:
	set(value):
		device_name = value
		if name_label:
			name_label.text = value

var price: int:
	set(value):
		price = value
		if price_label:
			price_label.text = str(value) + " credits"

@onready var name_label: Label = %NameLabel
@onready var price_label: Label = %PriceLabel


func _ready() -> void:
	name_label.text = device_name
	price_label.text = str(price) + " credits"


func on_money_change(value: int):
	disabled = value < price
	if disabled:
		price_label.add_theme_color_override("font_color", Color(0.824, 0, 0))
	else:
		price_label.add_theme_color_override("font_color", Color(0, 0.824, 0))
