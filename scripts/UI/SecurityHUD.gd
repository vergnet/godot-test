class_name SecurityHUD

extends CanvasLayer

signal start_device_placement(device: SecurityDevicesUtils.Devices)

const DEVICE_LIST_ITEM = preload("res://scenes/UI/components/SecurityDeviceListItem.tscn")

@export var security_player: SecurityPlayer

@onready var alerts_arrows: Control = $AlertsArrows
@onready var devices_list_vbox: VBoxContainer = %DevicesListVbox
@onready var money_label: Label = %MoneyLabel


func _ready() -> void:
	_generate_security_devices_list()
	assert(security_player, "No linked security player")
	security_player.money_changed.connect(_on_money_change)


func _on_money_change(value: int):
	money_label.text = "Money: " + str(value)


func _generate_security_devices_list():
	for c in devices_list_vbox.get_children():
		c.queue_free()
	for d in SecurityDevicesUtils.Devices.values():
		if d == SecurityDevicesUtils.Devices.NONE:
			continue
		var item: SecurityDeviceListItem = DEVICE_LIST_ITEM.instantiate()
		var device_data = SecurityDevicesUtils.get_device_data(d)
		item.device_name = device_data.device_name
		item.price = device_data.price
		item.pressed.connect(func(): _on_device_list_item_pressed(d))
		security_player.money_changed.connect(item.on_money_change)
		devices_list_vbox.add_child(item, true)
		item.on_money_change(security_player.security_money)


func _on_device_list_item_pressed(device: SecurityDevicesUtils.Devices):
	start_device_placement.emit(device)


func clear_alerts():
	for c in alerts_arrows.get_children():
		c.queue_free()


func add_alert(screen_pos: Vector2):
	var label = Label.new()
	label.text = "!"
	label.add_theme_color_override("font_color", Color(255, 0, 0))
	label.add_theme_constant_override("outline_size", 5)
	label.add_theme_font_size_override("font_size", 80)
	alerts_arrows.add_child(label, true)
	label.position = screen_pos
