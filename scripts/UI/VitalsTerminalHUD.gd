class_name VitalsTerminalHUD

extends CanvasLayer

const VITALS = preload("res://scenes/UI/components/InfiltratorVitalsContainer.tscn")

@export var root: VitalsTerminal

var players: PackedInt32Array:
	set(value):
		players = value
		for c: InfiltratorVitalsContainer in h_box_container.get_children():
			c.queue_free()
		for p in players:
			var vitals: InfiltratorVitalsContainer = VITALS.instantiate()
			vitals.player = p
			h_box_container.add_child(vitals, true)
			for c: Infiltrator in root.infiltrators_container.get_children():
				if c.player == p:
					vitals.infiltrator = c

@onready var h_box_container: HBoxContainer = $MarginContainer/VBoxContainer/HBoxContainer


func _ready() -> void:
	var peers = multiplayer.get_peers()
	peers.append(multiplayer.get_unique_id())
	players = peers
	multiplayer.peer_connected.connect(_on_new_player_connected)
	multiplayer.peer_disconnected.connect(_on_player_disconnected)
	root.infiltrators_container.child_entered_tree.connect(_on_infiltrator_spawned)
	root.infiltrators_container.child_exiting_tree.connect(_on_infiltrator_despawned)


func _on_new_player_connected(id: int):
	var array = players.duplicate()
	array.append(id)
	players = array


func _on_player_disconnected(id: int):
	var array = players.duplicate()
	array.remove_at(array.find(id))
	players = array


func _on_infiltrator_spawned(infiltrator: Infiltrator):
	# child_entered_tree is called before the player can be synced with the server
	infiltrator.player_changed.connect(_on_infiltrator_spawned)
	for c: InfiltratorVitalsContainer in h_box_container.get_children():
		if c.player == infiltrator.player:
			c.infiltrator = infiltrator


func _on_infiltrator_despawned(infiltrator: Infiltrator):
	for c: InfiltratorVitalsContainer in h_box_container.get_children():
		if c.player == infiltrator.player:
			c.infiltrator = null
