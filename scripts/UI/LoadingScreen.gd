class_name LoadingScreen

extends Control

@onready var loading_reason_label: Label = $CenterContainer/VBoxContainer/LoadingReasonLabel


func _ready() -> void:
	hide()


func show_loading(reason: String):
	loading_reason_label.text = reason
	show()
