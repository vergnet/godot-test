extends Control

var ui_theme = preload("res://resources/ui_theme.tres")

@onready var users_v_box_container: VBoxContainer = $MarginContainer/UsersVBoxContainer


func _ready() -> void:
	_update_members_list(SteamLobby.lobby_members)
	SteamLobby.on_lobby_members_update.connect(_update_members_list)


func _update_members_list(members: Array[SteamUser]):
	for child in users_v_box_container.get_children():
		child.queue_free()

	for m in members:
		var label = Label.new()
		label.text = m.name
		label.theme = ui_theme
		var font_size = 30
		if m.steam_id == GlobalSteam.steam_id:
			font_size = 40
		label.add_theme_font_size_override("font_size", font_size)
		users_v_box_container.add_child(label)
