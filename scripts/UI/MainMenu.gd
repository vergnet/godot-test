class_name MainMenu

extends Node3D

#var camera_schene = preload("res://scenes/playgrounds/SecurityCameraPlayground.tscn")
#var turret_schene = preload("res://scenes/playgrounds/SecurityTurretPlayground.tscn")

var open: bool:
	get:
		return visible
	set(value):
		if value:
			ui.show()
			show()
		else:
			loading_screen.hide()
			ui.hide()
			hide()

@onready var version_label: Label = %VersionLabel
@onready var steam_user_label: Label = %SteamUserLabel
@onready var lobby_line_edit: LineEdit = %SteamLobbyLineEdit
@onready var loading_screen: LoadingScreen = $UI/LoadingScreen
@onready var join_error_label: Label = %SteamJoinErrorLabel
@onready var ui: Control = $UI
@onready var lan_ip_line_edit: LineEdit = %LanIpLineEdit
@onready var steam_container: VBoxContainer = %Steam


func _ready() -> void:
	version_label.text = "Version " + ProjectSettings.get_setting("application/config/version")
	steam_user_label.text = "Welcome " + GlobalSteam.steam_username
	join_error_label.hide()
	SteamLobby.on_lobby_join_failed.connect(_on_lobby_join_failed)
	if not GlobalSteam.steam_available:
		steam_container.queue_free()


func _on_steam_host_button_pressed() -> void:
	loading_screen.show_loading("Creating lobby")
	SteamLobby.create_lobby()


func _on_camera_playground_button_pressed():
	#get_tree().change_scene_to_packed(camera_schene)
	pass


func _on_turret_playground_button_pressed():
	#get_tree().change_scene_to_packed(turret_schene)
	pass


func _on_setting_button_pressed():
	pass  # Replace with function body.


func _on_exit_button_pressed():
	get_tree().root.propagate_notification(NOTIFICATION_WM_CLOSE_REQUEST)
	get_tree().quit()


func _on_git_lab_logo_pressed():
	OS.shell_open("https://gitlab.com/vergnet/godot-test")


func _on_steam_join_button_pressed() -> void:
	if lobby_line_edit.text.is_valid_int():
		var lobby_id = lobby_line_edit.text.to_int()
		join_error_label.hide()
		loading_screen.show_loading("Joining lobby")
		SteamLobby.join_lobby(lobby_id)
	else:
		print("Invalid lobby ID")
		join_error_label.text = "Invalid lobby ID"
		join_error_label.show()


func _on_lobby_join_failed(_code: int, reason: String):
	loading_screen.hide()
	join_error_label.text = reason
	join_error_label.show()


func _on_lan_host_button_pressed() -> void:
	loading_screen.show_loading("Creating lobby")
	var peer = ENetMultiplayerPeer.new()
	peer.create_server(8080)
	if peer.get_connection_status() == MultiplayerPeer.CONNECTION_DISCONNECTED:
		OS.alert("Failed to start multiplayer server.")
		return
	multiplayer.multiplayer_peer = peer
	SteamLobby.on_lobby_join.emit()


func _on_lan_join_button_pressed() -> void:
	var ip = lan_ip_line_edit.text
	var peer = ENetMultiplayerPeer.new()
	peer.create_client(ip, 8080)
	if peer.get_connection_status() == MultiplayerPeer.CONNECTION_DISCONNECTED:
		OS.alert("Failed to start multiplayer client.")
		return
	multiplayer.multiplayer_peer = peer
	SteamLobby.on_lobby_join.emit()
