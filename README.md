# Godot test

This is a yet unnamed game made with Godot 4.

## Installation

No binaries available yet, you will need to run it with the game engine. See the next section.

## Developing

This game developed with Godot 4.2.1 and Blender 4.0.2. Make sure you first install both exact versions first before opening the project.

### Setting up model import

Models are saved as `.blend` files inside the project and are [automatically imported when the project is opened](https://docs.godotengine.org/en/stable/tutorials/assets_pipeline/importing_3d_scenes/available_formats.html#importing-blend-files-directly-within-godot) if you have Blender installed. Having the exact same blender version helps making sure the import process works as expected.

Once you have Godot and Blender installed, open the project. It will complain about missing assets because we have not yet configured Blender. Dismiss the modal then go to `Editor -> Editor Settings`. From here type `blender` in the search box. Select `FileSystem -> Import` in the left menu. Now enter in the `Blender 3 path` (even if you have installed Blender 4) your Blender installation folder. For example on Manjaro Linux with a version installed from the package manager you would enter `/usr/bin`.

Once this setup is done, close the project without saving and reopen it. Godot should now properly import the models.

### Settting up formatting/linting

This project uses the [GDScript Toolkit](https://github.com/Scony/godot-gdscript-toolkit) from Scony.
Extensions are pre-configured to run lint and format on save.

The CI will check the format and lint on every merge requests, so make sure you setup the checks locally to avoid headaches later on.

To work, you will need to first install the toolkit in the project:
```
# Create a venv in which to install the toolkit
python -m venv .venv
# Activate the venv for this terminal session
source .venv/bin/activate
# Install the toolkit
pip install "gdtoolkit==4.*"
```

To avoid having to enable the venv each time, you can make shortcuts in your local bin folder:
```
# Make sure you are running the commands from inside the project containing the venv
echo 'source '$(pwd)'/.venv/bin/activate && gdformat "$@"' > ~/.local/bin/gdformat && chmod +x ~/.local/bin/gdformat
echo 'source '$(pwd)'/.venv/bin/activate && gdlint "$@"' > ~/.local/bin/gdlint && chmod +x ~/.local/bin/gdlint
```
You can now use the commands `gdformat` and `gdlint` in any terminal and these will use the version installed in the project's venv.

## Docs

See the [`/docs/INDEX.md`](/docs/INDEX.md) file for more information on this project.
